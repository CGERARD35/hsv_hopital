# Étape 1: Construire l'application
FROM node:14
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY . /app
RUN npm run build -- --prod
