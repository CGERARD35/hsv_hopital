import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutMainComponent } from './layout-main/layout-main.component';
import {RouterOutlet} from "@angular/router";



@NgModule({
  declarations: [
    LayoutMainComponent
  ],
  imports: [
    CommonModule,
    RouterOutlet
  ],
  exports: [
    LayoutMainComponent
  ]
})
export class LayoutModule { }
