import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(private firestore: AngularFirestore) {}

  addBooking(booking: any) {
    return this.firestore.collection('bookings').add(booking);
  }
  getLatestBooking() {
    return this.firestore.collection('bookings', ref => ref
      .orderBy('createdAt', 'desc') // Assurez-vous que 'createdAt' est bien le champ correct
      .limit(1)
    ).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as any;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }
}
