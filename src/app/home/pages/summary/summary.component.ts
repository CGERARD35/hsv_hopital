import { Component } from '@angular/core';
import {BookingService} from "../../../core/services/booking.service";

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent {
  latestBooking: any;

  constructor(private bookingService: BookingService) {}

  ngOnInit(): void {
    this.bookingService.getLatestBooking().subscribe(data => {
      if (data.length > 0) {
        this.latestBooking = data[0];
      } else {
        console.log('Aucun rendez-vous trouvé');
      }
    });
  }
}
