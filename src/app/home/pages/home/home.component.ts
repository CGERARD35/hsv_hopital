import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {BookingService} from "../../../core/services/booking.service";
import {NgForm} from "@angular/forms";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  nom: string = '';
  prenom: string = '';
  date: string = '';
  heure: string = '';
  medecin: string = '';

    constructor(
      private router: Router,
      private bookingService: BookingService
    ) {}

    onSubmit(form: NgForm) {
      const bookingData = {
        nom: form.value.nom,
        prenom: form.value.prenom,
        date: form.value.date,
        heure: form.value.heure,
        medecin: form.value.medecin
      };

      this.bookingService.addBooking(bookingData).then(() => {
        console.log('Rendez-vous ajouté avec succès!');
        this.router.navigate(['/summary']);
      }).catch(error => {
        console.error('Erreur lors de l’ajout du rendez-vous:', error);
      });
    }

}
